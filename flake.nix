{
  description = "dualvoc";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    # inputs revlevant for devenv
    devenv.url = "github:cachix/devenv";
    nix2container.url = "github:nlewo/nix2container";
    nix2container.inputs.nixpkgs.follows = "nixpkgs";
    mk-shell-bin.url = "github:rrbutani/nix-mk-shell-bin";
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.devenv.flakeModule
      ];
      #systems = [ "x86_64-linux" "aarch64-linux" ];
      systems = [ "x86_64-linux" ];

      perSystem = { config, self', inputs', pkgs, system, ... }: {

        packages = {
          esptool = pkgs.esptool;
          picocom = pkgs.picocom;
          rshell = pkgs.rshell;
        };

        devenv =
        let
          envMicropython = {
            "PORT" = "/dev/ttyUSB0";
            "FW_URL" ="https://micropython.org/resources/firmware";
            "FW" = "ESP32_GENERIC-20231005-v1.21.0.bin";
          };
        in
        {
          shells = {

            mpy = {
              name = "micropython";
              env = envMicropython;
              packages = [
                config.packages.esptool
                config.packages.picocom
                config.packages.rshell
              ];

              enterShell = ''
                cd $DEVENV_ROOT/mpy
              '';

              scripts = {
                "dfw".exec = ''
                  wget $FW_URL/$FW
                '';

                "flash".exec = ''
                  esptool.py --chip esp32 --port $PORT erase_flash
                  esptool.py --chip esp32 --port $PORT --baud 460800 write_flash -z 0x1000 $FW
                  rm $FW
                '';

                "pico".exec = ''
                  picocom $PORT -b 115200
                '';

                "rconn".exec = ''
                  rshell -p $PORT
                '';
              };
            };

            client = {
              name = "dualvoc-client";
              languages.javascript = {
                enable = true;
                corepack.enable = true;
              };
              enterShell = ''
                cd $DEVENV_ROOT/client
              '';

              scripts = {
                "cin".exec = ''
                  npm install
                '';

                "cdev".exec = ''
                  npm run dev
                '';

                "cbuild".exec = ''
                  npm run build
                '';

                "cout".exec = ''
                  rm -rf .output/ dist/ node_modules/ .devenv/ .cache/
                '';
              };

            };

          };
        };
      };
      flake = {
        templates.default = { path = ./.; description = "dualvoc"; };
      };
    };
}
