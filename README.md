# ESP32 dualvoc

## Micropython ESP32
```
nix develop --impure .#mpy

# download micropython firmware
dfw

# flash to esp32
flash

# enter rshell
rshell -p /dev/ttyUSB0

```

## Web Client
```
nix develop --impure .#client

# install
cin

# run dev
cdev

# build
cbuild

# 'deploy'
rm -rf ../mpy/www/assets/*
cp -r dist/* ../mpy/www/

# cleanup
cout

```
