import { defineStore } from 'pinia';
import { useSystemStore } from './systemSettings.ts'
import { useStorage } from "@vueuse/core";

interface Sensor {
  T: number
  RH: number
  RAW: number
  VOC: number
}

interface SensorData {
  time: number
  board: Sensor
  cable: Sensor
}

export const useSensorStore = defineStore('sensor-data', {
  state: () => ({
    sensorData: useStorage('sensor-data', [] as SensorData[]),
    //sensorData: [] as SensorData[],
    socket: null as WebSocket | null,
    connected: false,
  }),
  actions: {
    createWebSocket() {
      if (!this.connected) {
        const url = useSystemStore().wsUrl;

        this.socket = new WebSocket(url);
        this.socket.onopen = this.handleOpen;
        this.socket.onmessage = this.handleMessage;
        this.socket.onerror = this.handleError;
        this.socket.onclose = this.handleClose;
      }
    },
    handleOpen() {
        console.log('WebSocket connection established');
        this.connected = true;
    },
    handleMessage(event: MessageEvent) {
        //console.log('Received message:', event.data);
        this.sensorData.push(JSON.parse(event.data));
    },
    handleError(event: Event) {
        console.error('WebSocket error:', event);
    },
    handleClose(event: CloseEvent) {
        console.log('WebSocket connection closed:', event);
        this.connected = false;
    },
    clear() {
        console.log('clearing data');
        this.sensorData = [];
    },
    download() {
        console.log('exporting data');
        var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(this.sensorData));
        var downloadAnchorNode = document.createElement('a');
        downloadAnchorNode.setAttribute("href",     dataStr);
        downloadAnchorNode.setAttribute("download", "data.json");
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    },
  }
});
