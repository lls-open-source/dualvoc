import { defineStore } from 'pinia';
import { useStorage } from "@vueuse/core";

interface Settings {
  ssid: string
  pw: string
  ip: string
  port: number
}

export const useSystemStore = defineStore( 'system-settings', {
  state: () => ({
    systemSettings: useStorage( 'system-settings', {
      ssid: "pls connect",
      pw: "pls connect",
      ip: "192.168.4.1",
      port: 80
    } as Settings),
    connected: false,
  }),
  actions: {
    update(settings: Settings) {
      this.systemSettings = settings;
    },
    save() {
      const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: this.jsonSettings
      };
      fetch(this.httpUrl, requestOptions)
    },
    async connect() {
      // get system data from device
      await fetch(this.httpUrl, { signal: AbortSignal.timeout(5000) }).then(response => {
        return response.json();
      }).then(data => {
        this.systemSettings = data;
        this.connected = true;
      }).catch(error => {
        console.log(error)
        this.connected = false;
      });
      // connect to websocket interface
    },
  },
  getters: {
    httpUrl: (state) => "http://" + state.systemSettings.ip + ":" + state.systemSettings.port + "/system",
    wsUrl: (state) => "ws://" + state.systemSettings.ip +  ":" + state.systemSettings.port + "/ws",
    jsonSettings: (state) => JSON.stringify(state.systemSettings)
  },
});
