import machine
import network
import asyncio
import json
from time import sleep
from microdot import Microdot, send_file
from microdot.websocket import with_websocket
from microdot.cors import CORS
#from microdot.sse import with_sse
from lls2 import LLS2

def gc_print():
    import gc
    """ """
    print('collecting garbage..')
    gc.collect()
    print("free: {} alloc: {}".format(gc.mem_free()/1000, gc.mem_alloc()/1000))

with open('system.json','r') as f:
    global system
    system = json.load(f)
    print("system.json: ", system)

"""
Network
"""
def do_ap():
    print('creating AP_IF')
    ap = network.WLAN(network.AP_IF)
    ap.active(True)
    ap.config(ssid='dualvoc')
    ap.config(max_clients=2)
    print('AP_IF config:', ap.ifconfig())

wlan = network.WLAN(network.STA_IF)
wlan.active(True)

scan = wlan.scan()

if system['ssid'] in [wifi[0].decode() for wifi in scan]:
    print('trying to connect to: ', system['ssid'])
    wlan.config(reconnects=3)
    wlan.connect(system['ssid'], system['pw'])
    while wlan.status() is network.STAT_CONNECTING:
        pass
    if wlan.status() is 205:
        print('failed connecting, falling back to AP_IF')
        wlan.active(False)
        do_ap()
    print('network config:', wlan.ifconfig())
else:
    wlan.active(False)
    do_ap()

gc_print()

"""
Webserver
"""
app = Microdot()
cors = CORS(app, allowed_origins='*')

@app.route('/')
async def index(request):
    return send_file('/www/index.html', max_age=86400)

@app.route('/system', methods=['GET', 'POST'])
async def system_settings(request):
    global system
    if request.method == 'GET':
        return json.dumps(system)
    elif request.method == 'POST':
        system = json.loads(request.body)
        with open('system.json','w') as f:
            json.dump(system, f)
        return '{"sucess": True}'

@app.route('/ws')
@with_websocket
async def send_sensors(request, ws):
    while True:
        await lock.wait()
        await ws.send(json.dumps(data))

@app.route('/<path:path>')
async def static(request, path):
    if '..' in path:
        # directory traversal is not allowed
        return 'Not found', 404
    return send_file('www/' + path, max_age=86400)

"""
Sensors
"""
async def read_sensors():
    global data
    data = {}
    counter = 0
    while True:
        lock.clear()
        await asyncio.sleep(0.8)
        board = sens_board.measure_all()
        cable = sens_cable.measure_all()
        data = {
                "time": counter,
                "board": { "T": board[0], "RH": board[1], "RAW": board[2], "VOC": board[3] },
                "cable": { "T": cable[0], "RH": cable[1], "RAW": cable[2], "VOC": cable[3] }
        }
        counter += 1
        lock.set()

i2c0 = machine.I2C(0, scl=machine.Pin(26), sda=machine.Pin(27))
i2c1 = machine.I2C(1, scl=machine.Pin(32), sda=machine.Pin(33))

print("I2C Board: ", i2c0.scan())
print("I2C Cable: ", i2c1.scan())

sens_board = LLS2(i2c0, True)
sens_cable = LLS2(i2c1, True)

#sens_board.selftest()
#sens_cable.selftest()

"""
Start tasks
"""
lock = asyncio.Event()
asyncio.create_task(read_sensors())

gc_print()

app.run(port=system['port'], debug=True)
