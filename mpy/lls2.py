from sgp40 import SGP40
from sht40 import SHT40
from voc_algo.voc_algo import VocAlgo

class LLS2(SGP40, SHT40, VocAlgo):
    """ """
    def __init__(self, i2c, check_crc=True):
        SGP40.__init__(self, i2c, check_crc)
        SHT40.__init__(self, i2c, check_crc)

        self.Sampling_Interval = 1.0
        VocAlgo.__init__(self, self.Sampling_Interval)

    def gc_print(self):
        import gc
        """ """
        print("free: {} alloc: {}".format(gc.mem_free()/1000, gc.mem_alloc()/1000))
        gc.collect()
        print("free: {} alloc: {}".format(gc.mem_free()/1000, gc.mem_alloc()/1000))

    def selftest(self):
        """ """
        print("------ Selftest ----------------")

        print("------ SHT40 -------------------")
        print("Serial Number: ", self.read_serial())

        temp, rh = self.measure_temp_rh(0)
        print("Temp [°C]: ", temp)
        print("RH [%]: ", rh)

        print("------ SGP40 -------------------")
        print("Serial Id: ", self.read_serial_id())

        ret = self.measure_test()
        print("Measure Test Success: ", ret)

        print("SRAW: ", self.measure_sraw())
        print("SRAW (compensated): ", self.measure_sraw_compensated())

        print("------ Selftest ----------------")

    def measure_sraw_compensated(self):
        """ """
        data = self.measure_temp_rh(0)
        sraw = self.measure_sraw(*data)
        return sraw

    def measure_voc_index(self):
        sraw = self.measure_sraw_compensated()
        return self.process(sraw)

    def measure_all(self):
        temp, rh = self.measure_temp_rh(0)
        sraw = self.measure_sraw(temp, rh)
        voc_index = self.process(sraw)
        return temp, rh, sraw, voc_index
