
class CommUtil:
    """ Utility functions for communicating with SGP40 and SHT40 """
    def __init__(self, i2c, check_crc=False):
        self.i2c = i2c
        self.check_crc = check_crc

    def soft_reset(self):
        """ Softreset of all devices """
        self.i2c.writeto(0x00, bytes(0x06))

    def _i2c_read(self, dev_addr, num_bytes, strip_crc=True):
        """ Read specified number of bytes from device - optionally strip the crc bytes """
        buf = bytearray(num_bytes)
        self.i2c.readfrom_into(dev_addr, buf)
        buf = list(buf)

        if num_bytes > 2 and self.check_crc:
            for i in range(0, num_bytes, 3):
                if not self.crc_correct(buf[i:i+2], buf[i+2]):
                    raise ValueError("Invalid CRC!")

        if strip_crc:
            for i in range(num_bytes, 0, -3):
                del buf[i-1]

        return buf

    def _i2c_write(self, dev_addr, command):
        """ Write command to device """
        #print("write: ", dev_addr, command)
        ret = self.i2c.writeto(dev_addr, bytes(command))
        if ret == len(command):
            return True
        else:
            return False

    def crc_correct(self, data, crc):
        """ Check data against crc """
        if crc == self.generate_crc(data):
            return True
        else:
            return False

    def generate_crc(self, data):
        """ Generate crc from data  """
        crc = 0xFF
        for byte in data:
            crc ^= byte
            for bit in range(8):
                if crc & 0x80:
                    crc = (crc << 1) ^ 0x31
                else:
                    crc = (crc << 1)
        return crc & 0xFF
