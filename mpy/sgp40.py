from comm_util import CommUtil
from time import sleep

_SGP40_ADDR = const(0x59)

class SGP40(CommUtil):
    def __init__(self, i2c, check_crc=False):
        super().__init__(i2c, check_crc)

    def measure_test(self):
        """ sensor measurement selftest """
        self._i2c_write(_SGP40_ADDR, [0x28, 0x0E])
        sleep(0.25)
        if self._i2c_read(_SGP40_ADDR, 3) == [0xd4, 0x00]:
            return True
        else:
            return False

    def read_serial_id(self):
        """ FIXME read serial id """
        self._i2c_write(_SGP40_ADDR, [0x36, 0x82])
        sleep(0.05)
        raw = self._i2c_read(_SGP40_ADDR, 3)
        ser_id = raw[0] << 8 | raw[0]
        return ser_id

    def measure_sraw(self, temp=None, rh=None):
        """ measure signal (SRAW) """
        cmd = [0x26, 0x0F]

        if temp and rh:
            cmd.extend(self.temp_rh_to_cmd(temp, rh))
        else:
            cmd.extend([0x80, 0x00, 0xA2, 0x66, 0x66, 0x93])

        self._i2c_write(_SGP40_ADDR, cmd)
        sleep(0.05)
        index = self._i2c_read(_SGP40_ADDR, 3)
        signal = index[0] << 8 | index[1]
        return signal

    def temp_rh_to_cmd(self, temp, rh):
        """ convert temp and rh to a command """
        temp_ticks = int((temp + 45) * (65535 / 175))
        rh_ticks = int(rh * (65535 / 125))

        temp_cmd = [(temp_ticks >> 8) & 0xFF, temp_ticks & 0xFF ]
        rh_cmd = [(rh_ticks >> 8) & 0xFF, rh_ticks & 0xFF ]

        temp_cmd.append(self.generate_crc(temp_cmd))
        rh_cmd.append(self.generate_crc(rh_cmd))

        temp_cmd.extend(rh_cmd)
        return temp_cmd

    def heater_off(self):
        """ turn off the heater -> idle mode """
        self._i2c_write(_SGP40_ADDR, [0x36, 0x15])
        sleep(0.01)
