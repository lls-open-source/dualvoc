class MoxModel:
    def __init__(self):
        self.Sraw_Std = 1.0
        self.Sraw_Mean = 0.0

    def set_parameters(self, std, mean):
        self.Sraw_Std = std
        self.Sraw_Mean = mean

    def process(self, sraw):
        """
        220 == SRAW_STD_BONUS
        230 == SRAW_INDEX_GAIN
        """
        return ((sraw - self.Sraw_Mean) / ( -(self.Sraw_Std + 220.0))) * 230.0

