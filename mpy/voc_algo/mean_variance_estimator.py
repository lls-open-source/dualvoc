from math import exp, sqrt

class MeanVarianceEstimator:
    def __init__(self, sampling_interval):
        self.Sampling_Interval = sampling_interval
        self.Gamma_Scaling = 64.0

        # set initial sigmoid parameters? or... don't ?
        self.sigmoid_set_parameters(0.0, 0.0, 0.0)

        self.Initialized = False

    def set_parameters(self, std_initial, tau_mean_variance_hours, gating_max_duration_minutes):
        self.Gating_Max_Duration_Minutes = gating_max_duration_minutes
        self.Mean = 0.0
        self.Sraw_Offset = 0.0
        self.Std = std_initial
        self.Gamma = ((self.Gamma_Scaling * (self.Sampling_Interval / 3600.)) /
            (tau_mean_variance_hours + (self.Sampling_Interval / 3600.)))
        self.Gamma_Initial_Mean = ((self.Gamma_Scaling * self.Sampling_Interval) /
            (20.0 + self.Sampling_Interval)) # 20.0 == TAU_INITIAL_MEAN
        self.Gamma_Initial_Variance = ((self.Gamma_Scaling * self.Sampling_Interval) /
            (2500.0 + self.Sampling_Interval)) # 2500.0 == TAU_INITIAL_VARIANCE
        self.Gamma_Mean = 0.0
        self.Gamma_Variance = 0.0
        self.Uptime_Gamma = 0.0
        self.Uptime_Gating = 0.0
        self.Gating_Duration_Minutes = 0.0

    def set_states(self, mean, std, uptime_gamma):
        self.Mean = mean
        self.Std = std
        self.Uptime_Gamma = uptime_gamma
        self.Initialized = True

    def get_std(self):
        return self.Std

    def get_mean(self):
        return self.Mean + self.Sraw_Offset

    def calculate_gamma(self, voc_index_from_prior):
        """ """
        # 32767.0 == FIX16_MAX
        uptime_limit = 32767.0 - self.Sampling_Interval

        if self.Uptime_Gamma < uptime_limit:
            self.Uptime_Gamma = self.Uptime_Gamma + self.Sampling_Interval

        if self.Uptime_Gating < uptime_limit:
            self.Uptime_Gating = self.Uptime_Gating + self.Sampling_Interval

        self.sigmoid_set_parameters(1.0, 2700.0, 0.01) # 1, INIT_DURATION_MEAN, INIT_TRANSITION_MEAN
        sigmoid_gamma_mean = self.sigmoid_process(self.Uptime_Gamma)
        gamma_mean = self.Gamma + ((self.Gamma_Initial_Mean - self.Gamma) * sigmoid_gamma_mean)
        # 340.0 == GATING_THRESHOLD
        # 510.0 == GATING_THRESHOLD_INITIAL
        gating_threshold_mean = 340.0 + ((510.0 - 340.0) * self.sigmoid_process(self.Uptime_Gating))

        # 0.09 == GATING_THRESHOLD_TRANSITION
        self.sigmoid_set_parameters(1.0, gating_threshold_mean, 0.09)
        sigmoid_gating_mean = self.sigmoid_process(voc_index_from_prior)
        self.Gamma_Mean = sigmoid_gating_mean * gamma_mean

        # 5220.0 == INIT_DURATION_VARIANCE
        # 0.01 == INIT_TRANSITION_VARIANCE
        self.sigmoid_set_parameters(1.0, 5220.0, 0.01)
        sigmoid_gamma_variance = self.sigmoid_process(self.Uptime_Gamma)
        gamma_variance = self.Gamma + ((self.Gamma_Initial_Variance - self.Gamma) * (sigmoid_gamma_variance - sigmoid_gamma_mean))
        gating_threshold_variance = 340.0 + ((510.0 - 340.0) * self.sigmoid_process(self.Uptime_Gating))

        self.sigmoid_set_parameters(1.0, gating_threshold_variance, 0.09)
        sigmoid_gating_variance = self.sigmoid_process(voc_index_from_prior)

        self.Gamma_Variance = sigmoid_gating_variance * gamma_variance
        # 0.3 == GATING_MAX_RATIO
        self.Gating_Duration_Minutes = self.Gating_Duration_Minutes + ((self.Sampling_Interval / 60.0) * (((1.0 - sigmoid_gating_mean) * (1. + 0.3)) - 0.3))

        if self.Gating_Duration_Minutes < 0.0:
            self.Gating_Duration_Minutes = 0.0

        if self.Gating_Duration_Minutes > self.Gating_Max_Duration_Minutes:
            self.Uptime_Gating = 0.0

    def process(self, sraw, voc_index_from_prior):
        """ """
        if not self.Initialized:
            self.Sraw_Offset = sraw
            self.Mean = 0.0
            self.Initialized = True
        else:
            if self.Mean >= 100.0 or self.Mean <= -100.0:
                self.Sraw_Offset = self.Sraw_Offset + self.Mean
                self.Mean = 0.0

            sraw = sraw - self.Sraw_Offset
            self.calculate_gamma(voc_index_from_prior)
            delta_sgp = self.Mean / self.Gamma_Scaling

            if delta_sgp < 0.0:
                c = self.Std - delta_sgp
            else:
                c = self.Std + delta_sgp

            additional_scaling = 1.0

            if c > 1440.0:
                additional_scaling = 4.0

            # FIXME!?!?!??!
            self.Std = sqrt((additional_scaling * (self.Gamma_Scaling - self.Gamma_Variance))) * sqrt((self.Std * (self.Std / (self.Gamma_Scaling * additional_scaling))) + ((self.Gamma_Variance * delta_sgp) / additional_scaling) * delta_sgp)
            self.Mean = self.Mean + (self.Gamma_Mean * delta_sgp)

    def sigmoid_set_parameters(self, L, K, X0):
        """ """
        self.Sigmoid_L = L
        self.Sigmoid_K = K
        self.Sigmoid_X0 = X0

    def sigmoid_process(self, sample):
        """ """
        x = self.Sigmoid_K * (sample - self.Sigmoid_X0)

        if x < -50:
            return self.Sigmoid_L
        elif x > 50:
            return 0.0
        else:
            return self.Sigmoid_L / (1 + exp(x))
