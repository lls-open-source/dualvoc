from math import exp

class SigmoidScaled:
    def __init__(self):
        self.Offset = 0.0

        self.Sigmoid_L = 500.0
        self.Sigmoid_K = -0.00065
        self.Sigmoid_X0 = 213.0

    def set_parameters(self, offset):
        self.Offset = offset

    def process(self, sample):

        x = self.Sigmoid_K * (sample - self.Sigmoid_X0)

        if x < -50.0:
            return self.Sigmoid_L
        elif x > 50.0:
            return 0.0
        else:
            if sample >= 0.0:
                shift = (self.Sigmoid_L - (5.0 * self.Offset)) / 4.0
                return ((self.Sigmoid_L + shift) / (1.0 + exp(x))) - shift
            else:
                # 100.0 == VOC_INDEX_OFFSET_DEFAULT
                return (self.Offset / 100.0) * (self.Sigmoid_L / (1.0 + exp(x)))
