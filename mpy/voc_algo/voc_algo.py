from math import exp
from .mean_variance_estimator import MeanVarianceEstimator
from .mox_model import MoxModel
from .sigmoid_scaled import SigmoidScaled
from .adaptive_lowpass import AdaptiveLowpass

class VocAlgo(MeanVarianceEstimator, MoxModel, SigmoidScaled, AdaptiveLowpass):
    """ """
    def __init__(self, sampling_interval):
        self.Sampling_Interval = sampling_interval

        self.Voc_Index_Offset = 100.0
        self.Tau_Mean_Variance_Hours = 12.0
        self.Gating_Max_Duration_Minutes = 60.0 * 3.0
        self.Sraw_Std_Initial = 50.0
        self.Uptime = 0.0
        self.Sraw = 0.0
        self.Voc_Index = 0.0

        self.Initial_Blackout = 45.0

        self.init_instances()

    def init_instances(self):
        """ """
        self.mve = MeanVarianceEstimator(self.Sampling_Interval)
        self.mve.set_parameters(self.Sraw_Std_Initial,
                self.Tau_Mean_Variance_Hours,
                self.Gating_Max_Duration_Minutes)

        self.mm = MoxModel()
        self.mm.set_parameters(self.mve.get_std(), self.mve.get_mean())

        self.ss = SigmoidScaled()
        self.ss.set_parameters(self.Voc_Index_Offset)

        self.lp = AdaptiveLowpass(self.Sampling_Interval)
        self.lp.set_parameters() # redundant?

    def get_states(self):
        """ """
        return self.mve.get_mean(), self.mve.get_std()

    def set_states(self, state0, state1):
        """ """
        self.mve.set_states(state0, state1, self.Persistence_Uptime_Gamma)
        self.Sraw = state0

    def set_tuning_parameters(self,
            voc_index_offset,
            learning_time_hours,
            gating_max_duration_minutes,
            std_initial):
        """ """

        self.Voc_Index_Offset = voc_index_offset
        self.Tau_Mean_Variance_Hours = learning_time_hours
        self.Gating_Max_Duration_Minutes = gating_max_duration_minutes
        self.Sraw_Std_Initial = std_initial

        self.init_instances()

    def process(self, sraw):
        """ """

        if self.Uptime <= self.Initial_Blackout:
            self.Uptime += self.Sampling_Interval
        else:
            if sraw > 0 and sraw < 65000:
                if sraw < 20001:
                    sraw = 20001
                elif sraw > 52767:
                    sraw = 52767

                self.Sraw = sraw - 20000

            self.Voc_Index = self.mm.process(self.Sraw)
            self.Voc_Index = self.ss.process(self.Voc_Index)
            self.Voc_Index = self.lp.process(self.Voc_Index)

            if self.Voc_Index < 0.5:
                self.Voc_Index = 0.5

            # self.Sraw is always > 0!
            if self.Sraw > 0:
                self.mve.process(self.Sraw, self.Voc_Index)
                self.mm.set_parameters(self.mve.get_std(), self.mve.get_mean())

        return self.Voc_Index + 0.5
