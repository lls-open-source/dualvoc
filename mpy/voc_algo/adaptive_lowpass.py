from math import exp

class AdaptiveLowpass:
    def __init__(self, sampling_interval):

        self.Initialized = False

        self.Sampling_Interval = sampling_interval

        self.A1 = None
        self.A2 = None
        self.X1 = None
        self.X2 = None
        self.X3 = None

        self.Tau_Fast = 20.0
        self.Tau_Slow = 500.0
        self.Alpha = -0.2

        self.set_parameters()

    def set_parameters(self):
        self.A1 = self.Sampling_Interval / (self.Tau_Fast + self.Sampling_Interval)
        self.A2 = self.Sampling_Interval / (self.Tau_Slow + self.Sampling_Interval)

    def process(self, sample):

        if not self.Initialized:
            self.X1 = sample
            self.X2 = sample
            self.X3 = sample
            self.Initialized = True

        self.X1 = ((1.0 - self.A1) * self.X1) + (self.A1 * sample)
        self.X2 = ((1.0 - self.A2) * self.X2) + (self.A2 * sample)

        abs_delta = self.X1 - self.X2

        if abs_delta < 0:
            abs_delta = -abs_delta

        F1 = exp(self.Alpha * abs_delta)

        tau_a = ((self.Tau_Slow - self.Tau_Fast) * F1) + self.Tau_Fast
        a3 = self.Sampling_Interval / (self.Sampling_Interval + tau_a)
        self.X3 = (( 1.0 - a3) * self.X3) + (a3 * sample)

        return self.X3

